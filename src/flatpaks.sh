flatpak install flathub io.github.Pithos -y
flatpak install flathub com.axosoft.GitKraken -y
flatpak install flathub com.slack.Slack -y
flatpak install flathub org.inkscape.Inkscape -y
flatpak install flathub org.kde.kdenlive -y
flatpak install flathub org.libreoffice.LibreOffice -y
flatpak install flathub com.github.gijsgoudzwaard.image-optimizer -y
flatpak install flathub com.github.PintaProject.Pinta -y
flatpak install flathub io.photoflare.photoflare -y
flatpak install flathub com.github.johnfactotum.Foliate -y

